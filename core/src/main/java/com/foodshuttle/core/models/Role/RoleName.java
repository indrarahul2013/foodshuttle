package com.foodshuttle.core.models.Role;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
