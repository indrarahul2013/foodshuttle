package com.foodshuttle.core.models;

public enum Gender {
    MALE,
    FEMALE,
    OTHERS
}
