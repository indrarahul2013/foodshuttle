package com.foodshuttle.core.repositories;

import com.foodshuttle.core.models.Role.Role;
import com.foodshuttle.core.models.Role.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RoleName name);
}
